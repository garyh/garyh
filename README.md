* <img src="https://gitlab.com/garyh/garyh/-/raw/main/images/black-tanuki.png" alt="Black Lives Matter" width="30"/> I believe that [Black Lives Matter](https://blacklivesmatter.com/resource-category/toolkits/)
* <img src="https://gitlab.com/garyh/garyh/-/raw/main/images/rainbow-tanuki.png" alt="Pride" width="30"/> I am proud of and I support our [diverse workforce](https://about.gitlab.com/company/culture/inclusion/)

# Gary's README

Hello! My name is Gary. I'm a Backend Engineer for GitLab's [Code Review](https://about.gitlab.com/handbook/engineering/development/dev/ecosystem/) team. I live in Chicago with a lovely wife, 2 adorable kids, and a really silly dog:

<img src="https://gitlab.com/garyh/garyh/-/raw/main/images/franklin.jpg" alt="Franklin"/>

---

My Links:
* [GitLab](https://www.gitlab.com/garyh)
* [LinkedIn](https://www.linkedin.com/in/gholtz/)
* [Twitter](https://twitter.com/garyholtz/)

Interested in other GitLab READMEs? Check out the [handbook](https://about.gitlab.com/handbook/engineering/readmes/)

## Work Style

* I live and work in the Central Time Zone.
  * Central Standard Time (UTC-6) in the winter
  * Central Daylight Time (UTC-5) in the summer.
* I try to make unblocking other people my number one priority.

## Communication Preferences

* I fully embrace [asynchronous communication](https://about.gitlab.com/company/culture/all-remote/asynchronous/):
  * You can Slack or email me at any time, but my responses may not be instantaneous
  * At the same time, I wouldn't expect your responses to be either.
* In GitLab, I only get notified of mentions of `@garyh`
* I almost always prefer a Slack message or GitLab comment over a Zoom call.
